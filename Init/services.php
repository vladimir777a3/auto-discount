<?php


namespace Okay\Modules\SimplaMarket\DiscountVariant\Init;


use Okay\Core\OkayContainer\Reference\ParameterReference AS PR;
use Okay\Core\OkayContainer\Reference\ServiceReference AS SR;
use Okay\Core\Design;
use Okay\Core\EntityFactory;
use Okay\Core\Request;
use Okay\Core\Settings;
use Okay\Modules\SimplaMarket\DiscountVariant\Extenders\BackendExtender;

return [
    BackendExtender::class => [
        'class' => BackendExtender::class,
        'arguments' => [
            new SR(Design::class),
            new SR(EntityFactory::class),
            new SR(Request::class),
        ],
    ],
    Init::class => [
        'class' => Init::class,
        'arguments' => [
            new SR(Settings::class),
        ],
    ],
];