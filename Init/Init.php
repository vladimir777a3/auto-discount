<?php


namespace Okay\Modules\SimplaMarket\DiscountVariant\Init;


use Okay\Admin\Helpers\BackendVariantsHelper;
use Okay\Core\Modules\AbstractInit;
use Okay\Core\ServiceLocator;
use Okay\Core\Settings;
use Okay\Entities\ProductsEntity;
use Okay\Entities\VariantsEntity;
use Okay\Modules\SimplaMarket\DiscountVariant\Extenders\BackendExtender;
use Okay\Core\Modules\EntityField;
use Okay\Admin\Requests\BackendProductsRequest;

class Init extends AbstractInit
{
    const PERMISSION = 'simpla_market__discount_variant';
    const DISCOUNT_VARIANT_FIELD = 'discount_variant';

    public function install()
    {
        $this->setBackendMainController('DiscountVariant');
        $this->migrateEntityField(VariantsEntity::class, (new EntityField(self::DISCOUNT_VARIANT_FIELD))->setTypeDecimal('6,2', true));
    }

    public function init()
    {
        $this->registerEntityField(VariantsEntity::class, self::DISCOUNT_VARIANT_FIELD);
        $this->addPermission(self::PERMISSION);
        $this->registerBackendController('DiscountVariant');
        $this->addBackendControllerPermission('DiscountVariant', self::PERMISSION);

        $this->registerQueueExtension(
            ['class' => BackendProductsRequest::class, 'method' => 'postVariants'],
            ['class' => BackendExtender::class, 'method' => 'postVariants']
        );

        $this->registerQueueExtension(
            ['class' => BackendVariantsHelper::class, 'method' => 'updateStocksAndPrices'],
            ['class' => BackendExtender::class, 'method' => 'updateDiscountVariant']
        );
    }
}