<?php


namespace Okay\Modules\SimplaMarket\DiscountVariant\Extenders;

use Okay\Core\Design;
use Okay\Core\Modules\Extender\ExtenderFacade;
use Okay\Core\Modules\Extender\ExtensionInterface;
use Okay\Core\EntityFactory;
use Okay\Core\Request;
use Okay\Core\Settings;
use Okay\Entities\VariantsEntity;

/**
 * @property Settings settings
 */
class BackendExtender implements ExtensionInterface
{
    /**
     * @var object
     */
    private $productsEntity;
    /**
     * @var object
     */
    private $variantsEntity;
    /**
     * @var object
     */
    private $request;

    public function __construct(
        Design $design,
        EntityFactory $entityFactory,
        Request $request
    )
    {
        $this->design = $design;
        $this->request = $request;
        $this->entityFactory = $entityFactory;
        $this->variantsEntity = $entityFactory->get(VariantsEntity::class);
    }

    public function postVariants($productVariants)
    {
        $variants_old = $this->variantsEntity->mappedBy('id')->find(['product_id' => $productVariants[0]->product_id]);
        $variants_new = $productVariants;

        $this->calcDiscountVariant($variants_old, $variants_new);
    }

    public function updateDiscountVariant($null, $prices)
    {
        $old_ids = array_keys($this->request->post('discount_variant'));
        $variants_old = $this->variantsEntity->mappedBy('id')->find(['id' => $old_ids]);

        if ($this->request->method('post')) {
            $discount_variants = $this->request->post('discount_variant');
            $variants_new = [];
            foreach ($discount_variants as $id => $discount_variant) {
                $variant_new = new \stdClass();
                $variant_new->id = $id;
                if (isset($this->request->post('price')[$id])) {
                    $variant_new->price = $this->request->post('price')[$id];
                } else {
                    $variant_new->price = $null;
                }
                if (isset($discount_variant)) {
                    $variant_new->discount_variant = $discount_variant;
                } else {
                    $variant_new->discount_variant = $null;
                }
                if (isset($variants_old[$id]->compare_price)) {
                    $variant_new->compare_price = $variants_old[$id]->compare_price;
                } else {
                    $variant_new->compare_price = $null;
                }
                $variants_new[] = $variant_new;
            }
        }
        $this->calcDiscountVariant($variants_old, $variants_new);
    }

    public function calcDiscountVariant($variants_old, $variants_new)
    {
        foreach ($variants_new as $key => $variant) {
            if (!empty($variant->discount_variant)) {
                $variant->discount_variant = $variant->discount_variant > 0 ? str_replace(',', '.', $variant->discount_variant) : null;
            } else {
                $variant->discount_variant = null;
            }

            if (($variant->price != 0) && ($variant->discount_variant != 0) && ($variant->compare_price == 0)) {
                $variant->compare_price = $variant->price;
                $variant->price = (100 - $variant->discount_variant) * $variant->compare_price / 100;
            } elseif (isset($variants_old[$variant->id])
                && ($variants_old[$variant->id]->price != 0) && ($variants_old[$variant->id]->discount_variant == 0) && ($variants_old[$variant->id]->compare_price != 0)
                && ($variant->price != 0) && ($variant->discount_variant != 0) && ($variant->compare_price != 0)) {
                $variant->price = (100 - $variant->discount_variant) * $variant->compare_price / 100;
            } elseif (isset($variants_old[$variant->id])
                && ($variants_old[$variant->id]->price != 0) && ($variants_old[$variant->id]->discount_variant != 0) && ($variants_old[$variant->id]->compare_price != 0)
                && ($variant->price != 0) && ($variant->discount_variant == 0) && ($variant->compare_price != 0)) {
                $variant->price = $variant->compare_price;
                $variant->compare_price = null;
            } elseif (isset($variants_old[$variant->id])
                && ($variants_old[$variant->id]->price == 0) && ($variants_old[$variant->id]->discount_variant == 100) && ($variants_old[$variant->id]->compare_price != 0)
                && ($variant->price == 0) && ($variant->discount_variant == 0) && ($variant->compare_price != 0)) {
                $variant->price = $variant->compare_price;
                $variant->compare_price = null;
            } elseif (isset($variants_old[$variant->id])
                && ($variants_old[$variant->id]->price != 0) && ($variants_old[$variant->id]->discount_variant != 0) && ($variants_old[$variant->id]->compare_price != 0)
                && ($variant->price != 0) && ($variant->discount_variant != 0) && ($variant->compare_price != 0)) {
                $variant->price = (100 - $variant->discount_variant) * $variant->compare_price / 100;
            }

            if (!empty($variant->id)) {
                $this->variantsEntity->update($variant->id, [
                    'price' => $variant->price,
                    'discount_variant' => $variant->discount_variant,
                    'compare_price' => $variant->compare_price,
                ]);
            }
        }
    }
}