<?php
$lang['simpla_market__discount_variant__module_title'] = "Название модуля";
$lang['simpla_market__discount_variant__module_description_title'] = "Описание";
$lang['simpla_market__discount_variant__module_description_content'] = "Модуль создает поле Скидка в Админке в Товаре в Варианте, и в Админке в списке Товаров.";
$lang['simpla_market__discount_variant__module_instruction_title'] = "Инструкция";
$lang['simpla_market__discount_variant__module_instruction_content'] = "Можно заполнить только часть полей. Часть остальных полей будет просчитана автоматически";