
    <div class="heading_label">{$btr->general_price|escape}</div>
    <input class="variant_input" name="variants[price][]" type="text" value="{$variant->price|escape}"{if $variant->price && $variant->discount_variant} disabled/>
    <input class="hidden" name="variants[price][]" type="text" value="{$variant->price|escape}"{/if}/>
</div>

<div class="okay_list_boding variants_item_price">
    <div class="heading_label">
        Скидка, %
        {*        Пояснение со знаком "?"*}
        {*        <i class="fn_tooltips" title="Позволяет рассчитать новое значение поля Цена, исходя из значения поля Старая цена">*}
        {*            {include file='svg_icon.tpl' svgId='icon_tooltips'}*}
        {*        </i>*}
    </div>
    <input class="variant_input" name="variants[discount_variant][]" type="text"
           value="{$variant->discount_variant|escape}"/>
    <input class="hidden" name="variants[product_id][]" type="text" value="{$variant->product_id|escape}"/>

