    <div class="input-group">
        <input class="form-control {if $product->variants[0]->compare_price > $product->variants[0]->price}text_warning{/if}"
               type="text" name="price[{$product->variants[0]->id}]" value="{$product->variants[0]->price}"{if $product->variants[0]->price && $product->variants[0]->discount_variant} disabled>
        <input class="hidden" name="price[{$product->variants[0]->id}]" type="text" value="{$product->variants[0]->price}"{/if}>
        <span class="input-group-addon">
   {if isset($currencies[$product->variants[0]->currency_id])}
       {$currencies[$product->variants[0]->currency_id]->code|escape}
   {/if}
   </span>
    </div>
</div>

<div class="okay_list_boding okay_list_price">
    <div class="input-group">
        <input class="form-control" type="text" name="discount_variant[{$product->variants[0]->id}]"
               value="{$product->variants[0]->discount_variant}">
        <span class="input-group-addon">
          %
     </span>
    </div>