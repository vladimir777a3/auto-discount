    <div class="input-group">
        <input class="form-control" type="text" name="price[{$variant->id}]" value="{$variant->price|escape}"{if $variant->price && $variant->discount_variant} disabled>
        <input class="hidden" name="price[{$variant->id}]" type="text" value="{$variant->price|escape}"{/if}>
        <span class="input-group-addon">
            {if isset($currencies[$variant->currency_id])}
                {$currencies[$variant->currency_id]->code}
            {/if}
        </span>
    </div>
</div>

<div class="okay_list_boding okay_list_price">
    <div class="input-group">
        <input class="form-control" type="text" name="discount_variant[{$variant->id}]"
               value="{$variant->discount_variant|escape}">
        <span class="input-group-addon">
            %
        </span>
    </div>