<?php


namespace Okay\Modules\SimplaMarket\DiscountVariant\Backend\Controllers;


use Okay\Admin\Controllers\IndexAdmin;

class DiscountVariant extends IndexAdmin
{
    public function fetch()
    {
        $this->response->setContent($this->design->fetch('module.tpl'));
    }
}